package st.rhapsody.ld38.entity.particle;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.Font;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public class TextParticle extends Particle{
    private String text;
    private Vector2 pos;

    public TextParticle(String text, Vector2 pos) {
        super(new Sprite(GameTexture.smallBullet));
        this.text = text;
        this.countdown = 190;
        this.pos = pos;
        direction = new Vector2(MathUtils.random(-0.5f,0.5f), MathUtils.random(+0.5f, 0.8f));
    }

    @Override
    public void tick(float delta) {
        if (pos != null) {
            pos.x += direction.x * (delta * 30);
            pos.y += direction.y * (delta * 30);
        }
        super.tick(delta);
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        Font.renderText(text,this.pos.x, this.pos.y,spriteBatch);
    }
}
