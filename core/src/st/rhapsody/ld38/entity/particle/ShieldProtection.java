package st.rhapsody.ld38.entity.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-23.
 */
public class ShieldProtection extends Particle{
    public ShieldProtection() {
        super(new Sprite(GameTexture.smallBullet));
        sprite.setColor(Color.WHITE);
        countdown = 190;
        sprite.setSize(8,8);
        direction = new Vector2(MathUtils.random(0.0f,1.0f), MathUtils.random(-0.7f, -1.0f));
    }

    @Override
    public void tick(float delta) {
        super.tick(delta);
        translate(direction.x * (delta*100), -direction.y * (delta*100));
        float scaleX = sprite.getScaleX();
        float scaleY = sprite.getScaleY();

        sprite.setScale(scaleX*0.98f, scaleY*0.98f);
    }
}
