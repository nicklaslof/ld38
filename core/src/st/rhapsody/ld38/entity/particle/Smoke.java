package st.rhapsody.ld38.entity.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-23.
 */
public class Smoke extends Particle{
    protected int speed = 10;
    public Smoke() {
        super(new Sprite(GameTexture.smallBullet));
        this.sprite.setSize(3,3);
        countdown = 200;
        direction = new Vector2(MathUtils.random(0.0f,1.0f), MathUtils.random(0.0f, 1.0f));
        sprite.setColor(Color.DARK_GRAY);
        sprite.setAlpha(0.5f);

    }

    @Override
    public void tick(float delta) {
        super.tick(delta);
        translate(direction.x * (delta*speed), -direction.y * (delta*speed));
        float scaleX = sprite.getScaleX();
        float scaleY = sprite.getScaleY();

        sprite.setScale(scaleX*1.01f, scaleY*1.01f);
        if (countdown < 120){
            sprite.setAlpha(0.3f);
        }
        if (countdown < 100) {
            sprite.setAlpha(0.2f);
        }
        if (countdown < 80){
            sprite.setAlpha(0.1f);
        }

        if (countdown < 60){
            sprite.setAlpha(0.05f);
        }

        if (countdown < 40){
            sprite.setAlpha(0.025f);
        }

        Color color = sprite.getColor();
        color.mul(1.015f);
        sprite.setColor(color);
    }
}
