package st.rhapsody.ld38.entity.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.entity.Entity;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public abstract class Particle extends Entity {
    protected Vector2 direction;
    protected int countdown;

    public Particle(Sprite sprite) {
        super(sprite);

    }

    @Override
    public void tick(float delta) {
        countdown--;
        if (countdown <= 0){
            setDispose();
        }
        super.tick(delta);
    }
}
