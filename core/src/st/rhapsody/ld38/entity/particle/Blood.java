package st.rhapsody.ld38.entity.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public class Blood extends Particle{
    public Blood() {
        super(new Sprite(GameTexture.smallBullet));
        this.sprite.setSize(32,32);
        countdown = 190;
        direction = new Vector2(MathUtils.random(0.0f,1.0f), MathUtils.random(0.0f, 1.0f));
        sprite.setColor(Color.RED);

    }

    @Override
    public void tick(float delta) {
        super.tick(delta);
        translate(direction.x * (delta*100), -direction.y * (delta*100));
        float scaleX = sprite.getScaleX();
        float scaleY = sprite.getScaleY();

        sprite.setScale(scaleX*0.98f, scaleY*0.98f);
    }
}
