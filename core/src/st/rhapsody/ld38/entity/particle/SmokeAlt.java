package st.rhapsody.ld38.entity.particle;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by nicklas on 2017-04-23.
 */
public class SmokeAlt extends Smoke{
    public SmokeAlt() {
        super();
        direction = new Vector2(MathUtils.random(-0.09f,0.09f), -1f);
        this.speed = 40;
        this.sprite.setSize(8,8);
    }
}
