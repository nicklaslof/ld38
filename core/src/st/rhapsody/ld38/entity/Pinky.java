package st.rhapsody.ld38.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.entity.bullet.Bullet;
import st.rhapsody.ld38.entity.bullet.DroppedBomb;
import st.rhapsody.ld38.entity.bullet.SeekerBomb;
import st.rhapsody.ld38.entity.particle.*;
import st.rhapsody.ld38.level.Level;
import st.rhapsody.ld38.resource.GameSound;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public class Pinky extends Enemy {
    private final float timeOffset;
    private final Color ufocolor;
    private float time;
    private static int counter;
    private int redCountdown;
    private float hurtCountdown;

    public Pinky() {
        super(new Sprite(GameTexture.pinky));
        counter++;

        timeOffset = counter / MathUtils.random(5f, 10f);

        ufocolor = new Color(0xe264a7ff);

        health = 2;
    }

    @Override
    public void tick(float delta) {
        time += delta;
        float frequency = 0.1f;
        float sin = MathUtils.sin(((time + timeOffset) * MathUtils.PI2) * frequency);
        float cos = MathUtils.cos(((time + timeOffset) * MathUtils.PI2) * frequency*3);
        float translateX = sin * (delta * 60);
        float translateY = cos * (delta * 60);

        translate(translateX, translateY);

        redCountdown--;
        hurtCountdown -= delta;

        if (redCountdown <= 0) {
            sprite.setColor(Color.WHITE);
        }

        if (MathUtils.random(0,800) == 1){
            if (Level.player != null) {
                //float v = MathUtils.atan2(Level.player.getY()-y, Level.player.getX() - x);
                //Vector2 direction = new Vector2(MathUtils.cosDeg(MathUtils.radiansToDegrees * v), MathUtils.sinDeg(MathUtils.radiansToDegrees * v)).nor();
                Vector2 direction = new Vector2(0, -1);
                Level.addBullet(new SeekerBomb(this, 70), new Vector2(x, y), direction);
            }
        }

        super.tick(delta);
    }

    @Override
    public void collided(Entity e2) {
        super.collided(e2);
        if (e2 instanceof Bullet) {
            if ((((Bullet)e2).getSource() instanceof Player) && !e2.isDispose()){
                if (hurtCountdown <= 0) {
                    sprite.setColor(Color.RED);
                    health--;

                    if (health <= 0) {
                        for (int i = 0; i < 50; i++) {
                            Particle particle = new PinkyDestroy();
                            Vector2 position = new Vector2(x + 16, y + 16);
                            particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                            Level.addParticle(particle);
                        }
                        //Level.addParticles(PinkyDestroy.class, new Vector2(x+16, y+16), 50);
                        Level.addParticle(new TextParticle("+150", new Vector2(x + 8, y + 16)));
                        Level.addScore(150);
                        //y += 40f;
                        GameSound.explode.play();
                        setDispose();

                    }else{
                        hurtCountdown = 1.0f;
                        redCountdown = 10;
                        for (int i = 0; i < 20; i++) {
                            Particle particle = new PinkyDestroy();
                            Vector2 position = new Vector2(x + 16, y + 16);
                            particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                            Level.addParticle(particle);
                        }
                        GameSound.partialHurt.play();
                    }

                    e2.setDispose();
                }
            }

        }
    }
}
