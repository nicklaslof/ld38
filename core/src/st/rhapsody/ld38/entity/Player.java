package st.rhapsody.ld38.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.Ld38;
import st.rhapsody.ld38.entity.bullet.Bullet;
import st.rhapsody.ld38.entity.particle.Blood;
import st.rhapsody.ld38.entity.particle.Particle;
import st.rhapsody.ld38.entity.particle.TextParticle;
import st.rhapsody.ld38.level.Level;
import st.rhapsody.ld38.powerup.*;
import st.rhapsody.ld38.resource.GameSound;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public class Player extends Entity{
    private final Sprite healthSprite;
    private final Sprite shieldSprite;
    float tmp;
    final Vector2 tmpV = new Vector2();
    private final Bezier<Vector2> bezier;
    //ImmediateModeRenderer20 renderer;
    private int redCountdown;
    private int hurtCountdown;
    private float rotY;

    //private Stack<Powerup> powerups = new Stack<Powerup>();
    private Powerup shieldPowerup;
    private Powerup bulletSpeed;
    private float rotation;
    private boolean doubleShoot;
    private boolean tripleShoot;


    public Player(Sprite sprite) {
        super(sprite);
        sprite.setSize(64,64);
        sprite.setOriginCenter();

        bezier = new Bezier<Vector2>(new Vector2(8, -10), new Vector2(Ld38.WIDTH / 2, Ld38.HEIGHT/5), new Vector2(Ld38.WIDTH-8, -10));
        bezier.approxLength(Ld38.WIDTH);
        //renderer = new ImmediateModeRenderer20(false, false, 0);

        maxHealth = 10;
        health = 10;

        healthSprite = new Sprite(GameTexture.pixel);
        shieldSprite = new Sprite(GameTexture.pixel);
        healthSprite.setOriginCenter();
        shieldSprite.setOriginCenter();

    }

    @Override
    public void translate(float xAmount, float yAmount) {

        tmp = bezier.locate(tmpV.set(this.x+xAmount, 0));
        bezier.valueAt(tmpV, tmp);

        super.setPosition(tmpV.x, tmpV.y);

    }

    @Override
    public void setPosition(float xAmount, float yAmount) {
        tmp = bezier.locate(tmpV.set(xAmount, 0));
        bezier.valueAt(tmpV, tmp);
        super.setPosition(tmpV.x, tmpV.y);
    }

    @Override
    public void tick(float delta) {

        if (health < 0){
            health = 0;
        }

        super.tick(delta);

        redCountdown--;
        hurtCountdown--;

        if (redCountdown <= 0) {
            sprite.setColor(Color.WHITE);
        }

        if (shieldPowerup != null && !shieldPowerup.isDispose()){
            shieldPowerup.tick(delta);
        }

        if (shieldPowerup != null && shieldPowerup.isDispose()){
            shieldPowerup = null;
        }

        if (bulletSpeed != null && !bulletSpeed.isDispose()){
            bulletSpeed.tick(delta);
        }

        if (bulletSpeed != null && bulletSpeed.isDispose()){
            bulletSpeed = null;
        }


        float rot = (getX()-(Ld38.WIDTH/2))/(Ld38.WIDTH/2);

        sprite.setRotation(-rot*20);
        healthSprite.setRotation(sprite.getRotation()/2);

        healthSprite.setPosition(x+6+(rot*10),y+80);
        healthSprite.setSize(5*health,10);

        healthSprite.setColor(Color.RED);


        if (maxHealth - health < 7){
            healthSprite.setColor(Color.ORANGE);
        }

        if (maxHealth - health < 5){
            healthSprite.setColor(Color.YELLOW);
        }

        if (maxHealth - health < 3){
            healthSprite.setColor(Color.GREEN);
        }


        if (hasShield() && shieldPowerup != null && shieldPowerup.getTimeToLive() > 0.0f){
            shieldSprite.setRotation(sprite.getRotation()/2);

            shieldSprite.setPosition(x+8+(rot*2),y-16);
            shieldSprite.setSize((shieldPowerup.getTimeToLive()*1.5f),10);

            shieldSprite.setColor(Color.WHITE);
        }

        healthSprite.setAlpha(0.3f);
        shieldSprite.setAlpha(0.3f);


    }

    public boolean hasShield(){
        return shieldPowerup != null && !shieldPowerup.isDispose();
    }

    public boolean hasBulletSpeed(){
        return bulletSpeed != null && !bulletSpeed.isDispose();
    }

    public boolean hasDoubleShoot() {
        return doubleShoot;
    }

    public boolean hasTripleShoot() {
        return tripleShoot;
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        super.render(spriteBatch);
        healthSprite.draw(spriteBatch);
        shieldSprite.draw(spriteBatch);

    }

    @Override
    public void collided(Entity e2) {
        super.collided(e2);
        if (e2 instanceof Bullet) {
            if (((Bullet) e2).getSource() != this) {
                if (hurtCountdown <= 0) {
                    sprite.setColor(Color.RED);
                    redCountdown = 10;
                    for (int i = 0; i < 20; i++) {
                        Particle particle = new Blood();
                        Vector2 position = new Vector2(x + 16, y + 16);
                        particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                        Level.addParticle(particle);
                    }
                    //Level.addParticles(Blood.class, new Vector2(x + 16, y + 16), 20);
                    //y += 40f;
                    hurtCountdown = 20;
                    Level.addParticle(new TextParticle("-10", new Vector2(x+8,y+64)));
                    Level.addScore(-10);
                    GameSound.playerHurt.play();
                    e2.setDispose();
                    health--;
                }
            }
        }
    }

    public void addPowerUp(Powerup powerup){
        if (powerup instanceof Shield){
            shieldPowerup = powerup;
            ShieldEntity shieldEntity = new ShieldEntity();
            shieldEntity.tick(0);
            Level.addEntity(shieldEntity,new Vector2(x,y));
        }else if (powerup instanceof BulletSpeed) {
            bulletSpeed = powerup;
            GameSound.speed.play();
        }else if (powerup instanceof HealthPowerup){
            if (health < maxHealth) {
                health += 3;
                GameSound.health.play();
            }
            if (health > maxHealth){
                health = maxHealth;
            }
        }else if (powerup instanceof DoubleShoot){
            doubleShoot = true;
            GameSound.weapon.play();
        }else if (powerup instanceof TripleShoot){
            tripleShoot = true;
            GameSound.weapon.play();
        }

    }

    public float getRotation() {
        return sprite.getRotation();
    }

    public boolean hasFullHealth() {
        return health == maxHealth;
    }

    public boolean isDead() {
        return health <= 0;
    }
}
