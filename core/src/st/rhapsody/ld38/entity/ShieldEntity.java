package st.rhapsody.ld38.entity;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.entity.bullet.Bullet;
import st.rhapsody.ld38.entity.particle.BomberDestroy;
import st.rhapsody.ld38.entity.particle.Particle;
import st.rhapsody.ld38.entity.particle.ShieldProtection;
import st.rhapsody.ld38.entity.particle.UfoDestroy;
import st.rhapsody.ld38.level.Level;
import st.rhapsody.ld38.resource.GameSound;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-23.
 */
public class ShieldEntity extends Entity{
    public ShieldEntity() {
        super(new Sprite(GameTexture.bubble));
        sprite.setSize(90,90);
    }

    @Override
    public void tick(float delta) {
        Player player = Level.player;
        float x = player.getX();
        float y = player.getY();

        this.setPosition(x-12,y);
        this.sprite.setRotation(player.getRotation());

        super.tick(delta);
    }

    @Override
    public void collided(Entity e2) {
        if (e2.isDispose()){
            return;
        }
        if (e2 instanceof Player || e2 instanceof ShieldEntity || e2 instanceof PowerupEntity || e2 instanceof Particle){
            return;
        }

        if (e2 instanceof Bullet){
            if (((Bullet)e2).getSource() == Level.player){
                return;
            }
        }

        e2.setDispose();
        for (int i = 0; i < 90; i++) {
            Particle particle = new ShieldProtection();
            Vector2 position = new Vector2(x + 16, y + 48);
            particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
            Level.addParticle(particle);
        }
        //Level.addParticles(ShieldProtection.class, new Vector2(x+16, y+48), 90);

        GameSound.shieldProtect.play();

    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        super.render(spriteBatch);
    }
}
