package st.rhapsody.ld38.entity;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.Ld38;

/**
 * Created by nicklas on 2017-04-22.
 */
public class Entity {

    protected Sprite sprite;
    float x;
    float y;
    private Rectangle rectangle;
    private Vector2 lerpPosition = new Vector2();
    private Vector2 finalPosition = new Vector2();
    private Vector2 previousLerpPosition = new Vector2();

    protected int health = 1;
    protected int maxHealth = 1;

    protected float rectangleOffset = 0;

    private boolean dispose;
    private boolean moveToFinalPosition;

    public Entity(Sprite sprite) {
        this.sprite = sprite;
        this.rectangle = new Rectangle();

    }

    public void tick(float delta){
        if (dispose){
            return;
        }

        if (moveToFinalPosition){
            lerpPosition.lerp(finalPosition, 0.01f);
            setPosition(lerpPosition.x, lerpPosition.y);
            previousLerpPosition.set(lerpPosition.x, lerpPosition.y);
            if (Math.abs(finalPosition.x-lerpPosition.x) < 10 && Math.abs(finalPosition.y-lerpPosition.y) < 10){
                moveToFinalPosition = false;
            }
        }


        this.sprite.setPosition(x,y);
        rectangle.setPosition(x,y);
        rectangle.setSize(sprite.getWidth()+rectangleOffset, sprite.getHeight()+rectangleOffset);



        if (!moveToFinalPosition && (x < -20 || x > Ld38.WIDTH+20 || y < -20 || y > Ld38.HEIGHT+20)){
            setDispose();
        }
    }

    public boolean isMoveToFinalPosition() {
        return moveToFinalPosition;
    }

    public void setDispose() {
        dispose = true;
    }

    public boolean isDispose() {
        return dispose;
    }

    public boolean doesCollide(Entity entity){
        return entity.rectangle.overlaps(this.rectangle);
    }

    public void render(SpriteBatch spriteBatch){
        sprite.draw(spriteBatch);
    }

    public void translate(float xAmount, float yAmount){
        x += xAmount;
        y += yAmount;
    }

    public float getX(){
        return x;
    }

    public float getY(){
        return y;
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
        this.lerpPosition.set(x,y);
    }

    public void setFinalPosition(float x, float y) {
        this.finalPosition.set(x,y);
        moveToFinalPosition = true;
    }

    public void collided(Entity e2) {

    }
}
