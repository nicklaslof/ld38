package st.rhapsody.ld38.entity.bullet;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.entity.Entity;
import st.rhapsody.ld38.entity.Pinky;
import st.rhapsody.ld38.entity.Player;
import st.rhapsody.ld38.entity.particle.*;
import st.rhapsody.ld38.level.Level;
import st.rhapsody.ld38.resource.GameSound;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public class SeekerBomb  extends Bullet{
    private int counter;
    private boolean correctRotation;
    private Vector2 currentDirection = new Vector2();
    private boolean soundPlayed;

    public SeekerBomb(Entity source, float speed) {
        super(source, new Sprite(GameTexture.seeker), speed);
        sprite.setSize(32,32);
        sprite.setOriginCenter();
        this.counter = 0;

    }

    @Override
    public void tick(float delta) {
        counter++;

        if (counter > 80) {
            if (!soundPlayed){
                GameSound.rocket.play(0.9f);
                soundPlayed = true;
            }
            float v = MathUtils.atan2(Level.player.getY() - getY(), Level.player.getX() - getX());

            if (correctRotation) {
                Vector2 direction = new Vector2(MathUtils.cosDeg(MathUtils.radiansToDegrees * v), MathUtils.sinDeg(MathUtils.radiansToDegrees * v)).nor();

                if (currentDirection.x != direction.x && currentDirection.y != direction.y) {
                    currentDirection.set(direction.x, direction.y);
                }

                Vector2 lerp = currentDirection.lerp(direction, 0.1f);

                setDirection(lerp.scl(1.5f));

            }

            float rot = (v * (180f / 3.1415927f)) + 90;
            int r = (int) rot;
            int r2 = (int) sprite.getRotation();

            if (r < r2){
                sprite.rotate(-delta*50);
                correctRotation = false;
            }else{
                correctRotation = true;
            }

            if (r > r2){
                sprite.rotate(delta*50);
                correctRotation = false;
            }else{
                correctRotation = true;
            }

            float rotation = sprite.getRotation();
            if (rotation > 0) {
                for (int i = 0; i < 2; i++) {
                    Particle particle = new Smoke();
                    Vector2 position = new Vector2(getX(), getY() + 16);
                    particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                    Level.addParticle(particle);
                }
                //Level.addParticles(Smoke.class, new Vector2(getX(), getY() + 16), 2);
            }

            if (rot< 0){
                for (int i = 0; i < 2; i++) {
                    Particle particle = new Smoke();
                    Vector2 position = new Vector2(getX() + 16, getY() + 16);
                    particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                    Level.addParticle(particle);
                }
                //Level.addParticles(Smoke.class, new Vector2(getX()+16, getY() + 16), 2);
            }

            //sprite.setRotation((v * (180f / 3.1415927f)) + 90);
        }


        super.tick(delta);
    }

    @Override
    public void collided(Entity e2) {
        if (e2 instanceof Bullet){
            Bullet b = (Bullet) e2;

            if (b.getSource() instanceof Player && !b.isDispose() && !isDispose()){
                setDispose();
                for (int i = 0; i < 20; i++) {
                    Particle particle = new PinkyDestroy();
                    Vector2 position = new Vector2(getX() + 16, getY() + 16);
                    particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                    Level.addParticle(particle);
                }
                //Level.addParticles(PinkyDestroy.class, new Vector2(getX()+16, getY()+16), 20);
                Level.addParticle(new TextParticle("+250", new Vector2(getX()+8,getY()+16)));
                Level.addScore(250);
                GameSound.explode.play();
                b.setDispose();
            }
        }
    }
}
