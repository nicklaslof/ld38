package st.rhapsody.ld38.entity.bullet;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.entity.Entity;
import st.rhapsody.ld38.entity.Player;
import st.rhapsody.ld38.entity.particle.*;
import st.rhapsody.ld38.level.Level;
import st.rhapsody.ld38.resource.GameSound;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public class DroppedBomb extends Bullet{
    public DroppedBomb(Entity source, float speed) {
        super(source, new Sprite(GameTexture.bomb), speed);
        sprite.setSize(40,40);
        sprite.setOriginCenter();


    }

    @Override
    public void tick(float delta) {


        super.tick(delta);

        sprite.rotate(delta*100);

        if (getY() < 15){
            for (int i = 0; i < 80; i++) {
                Particle particle = new WorldHurt();
                Vector2 position = new Vector2(getX(), getY());
                particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                Level.addParticle(particle);
            }
            //Level.addParticles(WorldHurt.class, new Vector2(getX(), getY()), 80);
            //Level.addParticles(SmokeAlt.class, new Vector2(getX()+16, getY()), 125);
            setDispose();
            Level.hurtWorld();
            Level.addParticle(new TextParticle("-300", new Vector2(getX()+8,getY()+16)));
            Level.addScore(-300);
            GameSound.worldHit.play();
        }
    }

    @Override
    public void collided(Entity e2) {

        super.collided(e2);
        if (e2 instanceof Bullet){
            Bullet b = (Bullet) e2;

            if (b.getSource() instanceof Player && !b.isDispose() && !isDispose()){
                setDispose();
                for (int i = 0; i < 20; i++) {
                    Particle particle = new UfoDestroy();
                    Vector2 position = new Vector2(getX() + 16, getY() + 16);
                    particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                    Level.addParticle(particle);
                }
                //Level.addParticles(UfoDestroy.class, new Vector2(getX()+16, getY()+16), 20);
                Level.addParticle(new TextParticle("+300", new Vector2(getX()+8,getY()+16)));
                Level.addScore(300);
                GameSound.explode.play();
                b.setDispose();
            }
        }
    }
}
