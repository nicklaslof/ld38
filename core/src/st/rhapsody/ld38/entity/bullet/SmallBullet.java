package st.rhapsody.ld38.entity.bullet;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import st.rhapsody.ld38.entity.Entity;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public class SmallBullet extends Bullet{
    public SmallBullet(Entity source, float speed, int size, Color color) {
        super(source, new Sprite(GameTexture.smallBullet), speed);
        sprite.setSize(size,size);
        sprite.setColor(color);
    }
}
