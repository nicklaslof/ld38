package st.rhapsody.ld38.entity.bullet;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.entity.Entity;
import st.rhapsody.ld38.entity.particle.BomberDestroy;
import st.rhapsody.ld38.entity.particle.TextParticle;
import st.rhapsody.ld38.level.Level;

/**
 * Created by nicklas on 2017-04-22.
 */
abstract public class Bullet extends Entity {
    private Vector2 direction;
    private Entity source;
    private float speed;

    public Bullet(Entity source, Sprite sprite, float speed) {
        super(sprite);
        this.source = source;
        this.speed = speed;
        rectangleOffset = 12;
    }

    public void setDirection(Vector2 direction) {

        this.direction = direction;
    }

    @Override
    public void tick(float delta) {
        translate(direction.x*(delta*speed), direction.y*+(delta*speed));

        super.tick(delta);
    }

    public Entity getSource() {
        return source;
    }


    @Override
    public void collided(Entity e2) {
        super.collided(e2);
    }
}
