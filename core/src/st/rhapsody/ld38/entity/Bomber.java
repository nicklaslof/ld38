package st.rhapsody.ld38.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.Ld38;
import st.rhapsody.ld38.entity.bullet.Bullet;
import st.rhapsody.ld38.entity.bullet.DroppedBomb;
import st.rhapsody.ld38.entity.particle.BomberDestroy;
import st.rhapsody.ld38.entity.particle.Particle;
import st.rhapsody.ld38.entity.particle.TextParticle;
import st.rhapsody.ld38.entity.particle.UfoDestroy;
import st.rhapsody.ld38.level.Level;
import st.rhapsody.ld38.resource.GameSound;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public class Bomber extends Enemy {
    private final float timeOffset;
    private final Color ufocolor;
    private float time;
    private static int counter;
    private int redCountdown;
    private float hurtCountdown;
    private int health;

    private boolean haveDroppedBomb;

    public Bomber() {
        super(new Sprite(GameTexture.bomber));
        counter++;

        timeOffset = counter / MathUtils.random(5f, 10f);

        ufocolor = new Color(0x171815ff);
        health = 2;
    }

    @Override
    public void tick(float delta) {
        time += delta;
        float frequency = 0.25f;
        float sin = MathUtils.sin(((time + timeOffset) * MathUtils.PI2) * frequency);
        float cos = MathUtils.cos(((time + timeOffset) * MathUtils.PI2) * frequency);
        float translateX = sin * (delta * 10);
        float translateY = cos * (delta * 10);

        translate(translateX, translateY);

        redCountdown--;
        hurtCountdown -= delta;

        if (redCountdown <= 0) {
            sprite.setColor(Color.WHITE);
        }

        if (!isMoveToFinalPosition() && !haveDroppedBomb){
            //float v = MathUtils.atan2(Level.player.getY()-y, Level.player.getX() - x);
            //Vector2 direction = new Vector2(MathUtils.cosDeg(MathUtils.radiansToDegrees * v), MathUtils.sinDeg(MathUtils.radiansToDegrees * v)).nor();
            Vector2 direction = new Vector2(0, -1);
            Level.addBullet(new DroppedBomb(this,90), new Vector2(x+32,y), direction);
            this.setFinalPosition(MathUtils.random(x-32, x+32), Ld38.HEIGHT+200);
            haveDroppedBomb = true;
        }

        super.tick(delta);
    }

    @Override
    public void collided(Entity e2) {
        super.collided(e2);
        if (e2 instanceof Bullet) {
            if ((((Bullet)e2).getSource() instanceof Player) && !e2.isDispose()){
                if (hurtCountdown <= 0) {
                    health--;
                    sprite.setColor(Color.RED);

                    if (health <= 0) {
                        for (int i = 0; i < 20; i++) {
                            Particle particle = new BomberDestroy();
                            Vector2 position = new Vector2(x + 16, y + 16);
                            particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                            Level.addParticle(particle);
                        }
                        //Level.addParticles(BomberDestroy.class, new Vector2(x+16, y+16), 20);
                        Level.addParticle(new TextParticle("+200", new Vector2(x + 8, y + 16)));
                        Level.addScore(200);
                        GameSound.explode.play();
                        //y += 40f;

                        setDispose();
                    }else{
                        hurtCountdown = 1.0f;
                        redCountdown = 10;
                        for (int i = 0; i < 5; i++) {
                            Particle particle = new BomberDestroy();
                            Vector2 position = new Vector2(x + 16, y + 16);
                            particle.setPosition(MathUtils.random(position.x - 5, position.x + 5), MathUtils.random(position.y - 5, position.y + 5));
                            Level.addParticle(particle);
                        }
                        GameSound.partialHurt.play();
                    }
                    e2.setDispose();
                }
            }

        }
    }
}
