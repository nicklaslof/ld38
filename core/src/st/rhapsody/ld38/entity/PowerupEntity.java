package st.rhapsody.ld38.entity;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.powerup.Powerup;

/**
 * Created by nicklas on 2017-04-23.
 */
public class PowerupEntity extends Entity{
    private Powerup powerup;

    public PowerupEntity(Powerup powerup) {
        super(new Sprite(powerup.getSpriteTexture()));
        this.powerup = powerup;
    }

    @Override
    public void tick(float delta) {

        translate(0, Vector2.Y.y * -(delta*100));
        super.tick(delta);


    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        super.render(spriteBatch);
    }

    @Override
    public void collided(Entity e2) {
        if (e2 instanceof Player && !isDispose()){
            ((Player)e2).addPowerUp(powerup);
            setDispose();
        }
    }
}
