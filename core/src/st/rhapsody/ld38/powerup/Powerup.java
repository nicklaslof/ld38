package st.rhapsody.ld38.powerup;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by nicklas on 2017-04-23.
 */
public class Powerup {
    private float timeToLive;
    private boolean dispose;
    private Texture spriteTexture;

    public Powerup(float timeToLive, Texture spriteTexture) {
        this.timeToLive = timeToLive;
        this.spriteTexture = spriteTexture;
    }

    public void tick(float delta){
        timeToLive -= delta;
        if (timeToLive <= 0.0f){
            dispose = true;
        }
    }


    public boolean isDispose() {
        return dispose;
    }

    public Texture getSpriteTexture() {
        return spriteTexture;
    }

    public float getTimeToLive() {
        return timeToLive;
    }
}
