package st.rhapsody.ld38.powerup;

import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-23.
 */
public class DoubleShoot extends Powerup{
    public DoubleShoot() {
        super(0, GameTexture.doubleShootPower);
    }
}
