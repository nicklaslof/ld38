package st.rhapsody.ld38.powerup;

import com.badlogic.gdx.graphics.Texture;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-23.
 */
public class BulletSpeed extends Powerup{
    public BulletSpeed() {
        super(40f, GameTexture.speedPower);
    }
}
