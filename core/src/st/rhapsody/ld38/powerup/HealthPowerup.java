package st.rhapsody.ld38.powerup;

import com.badlogic.gdx.graphics.Texture;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-24.
 */
public class HealthPowerup extends Powerup {
    public HealthPowerup() {
        super(0, GameTexture.healthPower);
    }
}
