package st.rhapsody.ld38.powerup;

import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-23.
 */
public class Shield extends Powerup{
    public Shield() {
        super(40f,  GameTexture.bubble);
    }
}
