package st.rhapsody.ld38.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld38.Font;
import st.rhapsody.ld38.Ld38;
import st.rhapsody.ld38.entity.Entity;
import st.rhapsody.ld38.entity.Pinky;
import st.rhapsody.ld38.entity.Ufo;
import st.rhapsody.ld38.entity.bullet.DroppedBomb;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-24.
 */
public class IntroScreen implements Screen {

    private SpriteBatch spriteBatch;
    private OrthographicCamera orthographicCamera;
    private float timeout;

    private Entity ufo;
    private Entity pinky;
    private Sprite droppedBomb;

    @Override
    public void show() {
        spriteBatch = new SpriteBatch();
        orthographicCamera = new OrthographicCamera(Ld38.WIDTH,Ld38.HEIGHT);
        timeout = 4.0f;
        new Ufo();
        ufo = new Ufo();
        ufo.setPosition(Ld38.WIDTH-280,Ld38.HEIGHT - 160);

        pinky = new Pinky();
        pinky.setPosition(220,Ld38.HEIGHT - 160);

        droppedBomb = new Sprite(GameTexture.bomb);

    }

    @Override
    public void render(float delta) {

        timeout -= delta;

        if (timeout <= 0.0f){
            if (Gdx.input.isButtonPressed(Input.Buttons.LEFT) || Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
                Ld38.setGameScreen();
            }
        }

        Gdx.gl.glClearColor(47f/255f,56f/255f,61f/255f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.setProjectionMatrix(orthographicCamera.combined);

        spriteBatch.begin();

        ufo.tick(delta);
        ufo.render(spriteBatch);

        pinky.tick(delta);
        pinky.render(spriteBatch);



        Font.renderText("A T T A C K   O N   S M O L W U R L D", (Ld38.WIDTH/2)-130, Ld38.HEIGHT-100, spriteBatch);


        Font.renderText("A   L U D U M   D A R E   3 8   G A M E", (Ld38.WIDTH/2)-128, Ld38.HEIGHT-160, spriteBatch);

        Font.renderText("P R O G R A M I N G, G R A P H I C S :  N I C K L A S   L O F", (Ld38.WIDTH/2)-200, Ld38.HEIGHT-210, spriteBatch);
        Font.renderText("G R A P H I C S, M U S I C :  M A T H I A S   L I N D F E L D T", (Ld38.WIDTH/2)-200, Ld38.HEIGHT-240, spriteBatch);


        droppedBomb.rotate(delta*100);

        droppedBomb.setPosition((Ld38.WIDTH/2)-70, Ld38.HEIGHT - 400);

        Font.renderText("W A T C H  O U T  F O R           T H E Y  W I L L  H U R T  S M O L W U R L D", (Ld38.WIDTH/2)-240, Ld38.HEIGHT-380, spriteBatch);
        Font.renderText("A N D  S M O L W U R L D   C A N T   B E  H E A L E D !", (Ld38.WIDTH/2)-170, Ld38.HEIGHT-420, spriteBatch);

        Font.renderText("[A]   A N D   [D]  T O  M O V E.   A I M  A N D  S H O O T  W I T H  T H E  M O U S E", (Ld38.WIDTH/2)-250, Ld38.HEIGHT-500, spriteBatch);

        droppedBomb.draw(spriteBatch);

        spriteBatch.end();
    }

    @Override
    public void resize(int width, int height) {
        show();
        orthographicCamera.position.set((Ld38.WIDTH/2)+8,Ld38.HEIGHT/2,0);
        orthographicCamera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
