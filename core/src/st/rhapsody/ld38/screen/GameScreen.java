package st.rhapsody.ld38.screen;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld38.Font;
import st.rhapsody.ld38.Ld38;
import st.rhapsody.ld38.input.InputController;
import st.rhapsody.ld38.level.Level;
import st.rhapsody.ld38.resource.GameSound;
import st.rhapsody.ld38.resource.GameTexture;


/**
 * Created by nicklas on 2017-04-22.
 */
public class GameScreen implements Screen {
    private SpriteBatch spriteBatch;
    private OrthographicCamera orthographicCamera;

    private InputController inputController;
    private Level level;
    private Sprite overlay = new Sprite(GameTexture.pixel);
    private Sprite doubleShoot = new Sprite(GameTexture.doubleShootPower);
    private Sprite trippleShoot = new Sprite(GameTexture.tripleShootPower);



    @Override
    public void show() {
        spriteBatch = new SpriteBatch();
        orthographicCamera = new OrthographicCamera(Ld38.WIDTH,Ld38.HEIGHT);

        inputController = new InputController();

        Gdx.input.setInputProcessor(inputController);

        level = new Level(inputController);


        Gdx.input.setCursorCatched(true);
        Gdx.input.setCursorPosition(Ld38.WIDTH/2, Ld38.HEIGHT/2);

        overlay.setAlpha(0.0f);
        overlay.setColor(Color.BLACK);
        overlay.setSize(Ld38.WIDTH+8, Ld38.HEIGHT);


    }

    @Override
    public void render(float delta) {

        if (level.worldIsDead()){
            GameSound.gameOver.play(0.6f);
            Ld38.setGameoverScreen(GameoverScreen.Type.WORLDDEATH, level.getScore());
        }

        if (level.player.isDead()){
            GameSound.gameOver.play(0.6f);
            Ld38.setGameoverScreen(GameoverScreen.Type.PLAYERDEATH, level.getScore());
        }

        if (level != null && !inputController.isGamePaused() && !level.worldIsDead()){
            level.tick(delta);
        }

        if (inputController.isGamePaused()){
            overlay.setAlpha(0.4f);
        }

        Gdx.gl.glClearColor(0,0,0,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.setProjectionMatrix(orthographicCamera.combined);

        spriteBatch.begin();


        if (level != null && !level.worldIsDead()){
            level.render(spriteBatch);
        }


        drawPowerups(spriteBatch);

        drawScore(spriteBatch);


        if (inputController.isGamePaused() && !level.worldIsDead()) {
            overlay.draw(spriteBatch);
            Font.renderText("PAUSED", (Ld38.WIDTH/2)-20, Ld38.HEIGHT/2, spriteBatch);
        }

        spriteBatch.end();


    }

    private void drawScore(SpriteBatch spriteBatch){
        int score = level.getScore();

        Font.renderText("SCORE: "+String.valueOf(score), 16, 48, spriteBatch);
    }

    private void drawPowerups(SpriteBatch spriteBatch) {
        if (level.worldIsDead()){
            return;
        }
        if (level.player.hasDoubleShoot()){
            doubleShoot.setAlpha(1.0f);
        }else{
            doubleShoot.setAlpha(0.3f);
        }

        if (level.player.hasTripleShoot()){
            trippleShoot.setAlpha(1.0f);
        }else{
            trippleShoot.setAlpha(0.3f);
        }

        doubleShoot.setPosition(Ld38.WIDTH-128,8);
        trippleShoot.setPosition(Ld38.WIDTH-64, 8);

        doubleShoot.draw(spriteBatch);
        trippleShoot.draw(spriteBatch);


    }

    @Override
    public void resize(int width, int height) {
        show();
        orthographicCamera.position.set((Ld38.WIDTH/2)+8,Ld38.HEIGHT/2,0);
        orthographicCamera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
