package st.rhapsody.ld38.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld38.Font;
import st.rhapsody.ld38.Ld38;

/**
 * Created by nicklas on 2017-04-24.
 */
public class GameoverScreen implements Screen{
    private Type type;
    private final int finalScore;
    private SpriteBatch spriteBatch;
    private OrthographicCamera orthographicCamera;
    private float timeout;

    public GameoverScreen(Type type, int finalScore) {

        this.type = type;
        this.finalScore = finalScore;
    }

    @Override
    public void show() {
        spriteBatch = new SpriteBatch();
        orthographicCamera = new OrthographicCamera(Ld38.WIDTH,Ld38.HEIGHT);

        timeout = 4.0f;

    }

    @Override
    public void render(float delta) {

        timeout -= delta;

        if (timeout <= 0.0f){
            if (Gdx.input.isButtonPressed(Input.Buttons.LEFT) || Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
                Ld38.setIntroScreen();
            }
        }

        Gdx.gl.glClearColor(0,0,0,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.setProjectionMatrix(orthographicCamera.combined);

        spriteBatch.begin();
        Font.renderText("GAME OVER!", (Ld38.WIDTH/2)-32, Ld38.HEIGHT/2, spriteBatch);
        switch (type){
            case PLAYERDEATH:
                Font.renderText("YOU DIED AND YOUR SMALL PLANET WILL BE NEXT", (Ld38.WIDTH/2)-170, (Ld38.HEIGHT/2)-32, spriteBatch);
                break;
            case WORLDDEATH:
                Font.renderText("YOU FAILED TO SAVE YOUR SMALL PLANET!", (Ld38.WIDTH/2)-132, (Ld38.HEIGHT/2)-32, spriteBatch);
                break;
        }


        Font.renderText("FINAL SCORE: "+String.valueOf(finalScore), (Ld38.WIDTH/2)-(32+(String.valueOf(finalScore).length()*6)), (Ld38.HEIGHT/2)-96, spriteBatch);

        spriteBatch.end();
    }

    @Override
    public void resize(int width, int height) {
        show();
        orthographicCamera.position.set((Ld38.WIDTH/2)+8,Ld38.HEIGHT/2,0);
        orthographicCamera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public enum Type{
        PLAYERDEATH,
        WORLDDEATH;
    }
}
