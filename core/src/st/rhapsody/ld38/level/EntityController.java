package st.rhapsody.ld38.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld38.Ld38;
import st.rhapsody.ld38.entity.*;
import st.rhapsody.ld38.entity.bullet.Bullet;
import st.rhapsody.ld38.entity.particle.Particle;
import st.rhapsody.ld38.powerup.*;

/**
 * Created by nicklas on 2017-04-22.
 */
public class EntityController {
    private Array<Entity> entities = new Array<Entity>();
    private Array<Entity> bullets = new Array<Entity>();
    private Array<Entity> particles = new Array<Entity>();


    private Array<Entity> tempArray = new Array<Entity>();

    private Array<Entity> toDispose = new Array<Entity>();


    private Player player;
    private float counter;
    private Entity playerShield;

    private float diffcultCounter = 0;

    private int difficult = 1;

    private int enimies = 0;

    public EntityController(Player player) {

        this.player = player;
        entities.add(player);
    }

    public void tick(float delta) {

        if (toDispose.size > 0){
            toDispose.clear();
        }

        diffcultCounter += delta;

        if (diffcultCounter > 50){
            difficult++;
            diffcultCounter = 0.0f;
        }


        if (enimies < difficult){
            if (MathUtils.random(0,60) == 1) {
                int random = MathUtils.random(0, 2);
                Entity e = null;
                switch (random) {
                    case 0:
                        e = addUfo();
                        break;
                    case 1:
                        e = addBomber();
                        break;
                    case 2:
                        e = addPinky();
                        break;
                }

                entities.add(e);
                enimies++;
            }
        }

        int random = MathUtils.random(1, 6000);

        PowerupEntity powerup = null;

        if (random < 10){
            if (Level.getDifficult() > 3 && !player.hasDoubleShoot()){
                powerup = new PowerupEntity(new DoubleShoot());
            }

            if (Level.getDifficult() > 6 && !player.hasTripleShoot()){
                powerup = new PowerupEntity(new TripleShoot());
            }
        }

        if (random == 11 && !player.hasBulletSpeed()){
            powerup = new PowerupEntity(new BulletSpeed());
        }

        if (random == 12 && !player.hasShield()){
            powerup = new PowerupEntity(new Shield());
        }

        if (random == 13 && !player.hasFullHealth()){
            powerup = new PowerupEntity(new HealthPowerup());
        }


        if (powerup != null) {
            Level.addEntity(powerup, new Vector2(MathUtils.random(32,Ld38.WIDTH-32), Ld38.HEIGHT-32));
        }



        for (Entity entity : entities) {
            entity.tick(delta);
            if (entity.isDispose()){
                toDispose.add(entity);
                if (entity instanceof Enemy){
                    enimies--;
                }
            }
        }

        for (Entity bullet : bullets) {
            bullet.tick(delta);
            if (bullet.isDispose()){
                toDispose.add(bullet);
            }
        }



        collisionCheck(delta);


        for (Entity particle : particles) {
            particle.tick(delta);
            if (particle.isDispose()){
                toDispose.add(particle);
            }
        }

        entities.removeAll(toDispose, true);
        bullets.removeAll(toDispose, true);
        particles.removeAll(toDispose, true);

        counter += delta;

        if (counter > 2){
            counter = 0;
            System.out.println("Enimies:" + enimies+"  " + "Entities:"+entities.size+" Bullets:"+bullets.size+ " Particles"+ particles.size +" FPS: "+ Gdx.graphics.getFramesPerSecond());
        }

    }

    private Entity addPinky() {
        Entity e;
        e = new Pinky();
        int x = MathUtils.random(-32, Ld38.WIDTH + 32);
        int y;
        if (x < 0 || x > Ld38.WIDTH){
            y = MathUtils.random(Ld38.HEIGHT-50,Ld38.HEIGHT);
        }else{
            y = MathUtils.random(Ld38.HEIGHT+32, Ld38.HEIGHT+100);
        }
        e.setPosition(x, y);
        e.setFinalPosition(MathUtils.random(32, Ld38.WIDTH-128), MathUtils.random(Ld38.HEIGHT/2,Ld38.HEIGHT-128));
        return e;
    }

    private Entity addBomber() {
        Entity e = new Bomber();
        int x = MathUtils.random(0+32, Ld38.WIDTH-140);
        int y = Ld38.HEIGHT;


        e.setPosition(x, y);
        e.setFinalPosition(x, y-MathUtils.random(128,300));
        return e;
    }

    private Entity addUfo() {
        Entity e;
        e = new Ufo();
        int x = MathUtils.random(-32, Ld38.WIDTH + 32);
        int y;
        if (x < 0 || x > Ld38.WIDTH){
            y = MathUtils.random(Ld38.HEIGHT-50,Ld38.HEIGHT);
        }else{
            y = MathUtils.random(Ld38.HEIGHT+32, Ld38.HEIGHT+100);
        }
        e.setPosition(x, y);
        e.setFinalPosition(MathUtils.random(32, Ld38.WIDTH-128), MathUtils.random(Ld38.HEIGHT/2,Ld38.HEIGHT-128));
        return e;
    }

    private void collisionCheck(float delta) {
        if (tempArray.size > 0){
            tempArray.clear();
        }

        tempArray.addAll(bullets);
        tempArray.addAll(entities);
        tempArray.addAll(particles);


        for (Entity entity : entities) {
            for (Entity e2 : tempArray) {
                if (entity != e2) {
                    if (entity.doesCollide(e2)) {
                        entity.collided(e2);
                    }
                }
            }
        }

        if (tempArray.size > 0){
            tempArray.clear();
        }

        tempArray.addAll(bullets);

        for (Entity entity : bullets) {
            for (Entity e2 : tempArray) {
                if (entity != e2) {
                    if (entity.doesCollide(e2)) {
                        entity.collided(e2);
                    }
                }
            }
        }

    }

    public void render(SpriteBatch spriteBatch) {

        for (Entity bullet : bullets) {
            bullet.render(spriteBatch);
        }

        for (Entity entity : entities) {
            if (!(entity instanceof ShieldEntity)){
                entity.render(spriteBatch);
            }

        }

        for (Entity particle : particles) {
            particle.render(spriteBatch);
        }
    }

    public void renderPlayerShield(SpriteBatch spriteBatch){
        if (playerShield == null){
            return;
        }
        if (player.hasShield()){
            //System.out.println("Player has shield");
            playerShield.render(spriteBatch);
        }else{
            playerShield.setDispose();
        }
    }

    public void addBullet(Bullet bullet, Vector2 postion, Vector2 direction) {

        bullet.setPosition(postion.x, postion.y);
        bullet.setDirection(direction);
        bullets.add(bullet);

    }

    public void addParticle(Particle particle){
        particles.add(particle);

    }


    public void addParticles(Array<Particle> particles, Vector2 postion, int number) {
        particles.addAll(particles);


    }

    public void addEntity(Entity entity, Vector2 vector2) {
        entity.setPosition(vector2.x, vector2.y);

        if (entity instanceof ShieldEntity){
            if (playerShield != null) {
                entities.removeValue(playerShield, true);
            }
            playerShield = entity;
        }
            entities.add(entity);

    }

    public int getDifficult() {
        return difficult;
    }
}
