package st.rhapsody.ld38.level;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.Ld38;

/**
 * Created by nicklas on 2017-04-23.
 */
abstract public class Background {
    private final Bezier<Vector2> bezier;
    private Sprite sprite;
    float tmp;
    final Vector2 tmpV = new Vector2();
    private boolean dispose = false;

    private Vector2 position;
    private float yOffset;

    public Background(Sprite sprite, Vector2 position, float yOffset) {
        this.sprite = sprite;
        this.position = position;
        this.yOffset = yOffset;
        bezier = generateBezier();
        this.sprite.setOrigin(0,0);

        //sprite.setAlpha(0.8f);
    }

    public void tick(float delta) {

        float xAmount = delta*50;

        tmp = bezier.locate(tmpV.set(position.x-xAmount, 0));
        bezier.valueAt(tmpV, tmp);

        position.set(tmpV.x, tmpV.y);


        if (position.x <= -320){
            dispose = true;
        }


        sprite.setPosition(position.x, position.y+yOffset);


        float rot = (position.x-(Ld38.WIDTH/2))/(Ld38.WIDTH/2);
        //System.out.println(rot);
        sprite.setRotation(-rot*10);

    }

    protected void setTexture(Texture texture){
        sprite.setTexture(texture);
    }

    public void render(SpriteBatch spriteBatch) {
        //System.out.printf(sprite.getTexture().toString());
        sprite.draw(spriteBatch);
    }

    public boolean isDispose() {
        return dispose;
    }

    public abstract Bezier<Vector2> generateBezier();
}
