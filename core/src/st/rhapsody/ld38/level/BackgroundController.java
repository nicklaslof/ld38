package st.rhapsody.ld38.level;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld38.Ld38;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-23.
 */
public class BackgroundController {

    private Array<Background> backgrounds = new Array<Background>();
    private Array<Background> toDispose = new Array<Background>();

    public void tick(float delta){


        int random = MathUtils.random(0, 400);

        if (random == 400){

            Texture texture = null;

            random = MathUtils.random(0, 2);



            float offset = MathUtils.random(0f,40f);
            //float offset = 0;

            MountainBackground mountainBackground = new MountainBackground(texture,new Vector2(Ld38.WIDTH, Ld38.HEIGHT),offset);
            backgrounds.add(mountainBackground);
        }

        if (toDispose.size > 0){
            toDispose.clear();
        }

        for (Background background : backgrounds) {
            background.tick(delta);
            if (background.isDispose()){
                toDispose.add(background);
            }
        }

        backgrounds.removeAll(toDispose, true);

    }

    public void render(SpriteBatch spriteBatch){
        for (Background background : backgrounds) {
            background.render(spriteBatch);
        }
    }

}
