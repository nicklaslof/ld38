package st.rhapsody.ld38.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import st.rhapsody.ld38.Ld38;
import st.rhapsody.ld38.entity.Entity;
import st.rhapsody.ld38.entity.Player;
import st.rhapsody.ld38.entity.bullet.Bullet;
import st.rhapsody.ld38.entity.bullet.SmallBullet;
import st.rhapsody.ld38.entity.particle.Particle;
import st.rhapsody.ld38.input.InputController;
import st.rhapsody.ld38.resource.GameSound;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-22.
 */
public class Level {

    public static Player player;
    private static EntityController entityController;
    //private BackgroundController backgroundController;
    private Sprite ground;
    private Sprite smallWorld2;
    private Sprite crossHair;

    private InputController inputController;
    private final Vector2 crossHairLocation = new Vector2();
    private Sprite ground2;
    private float shootTimeout;
    private Sprite stars;
    private Sprite sun;

    private static int score;

    private ArrayMap<Integer, Color> colors = new ArrayMap<Integer, Color>(20);

    private Color worldColor = Color.WHITE.cpy();

    private static int worldHealth;
    private static int previousWorldHealth = worldHealth;

    private static float worldShake = 0.0f;
    private Vector2 groundNormalPosition;

    private float shakeX;
    private float shakeY;

    public Level(InputController inputController) {
        this.inputController = inputController;
        Sprite sprite = new Sprite(GameTexture.hero);

        this.player = new Player(sprite);

        this.player.setPosition(Ld38.WIDTH / 2, 0);


        setupBackround();

        setupCrosshair();


        entityController = new EntityController(this.player);
        //backgroundController = new BackgroundController();
        worldHealth = 20;
        worldShake = 0.0f;
        score = 0;

        colors.put(20, new Color(1f, 1f, 1f, 1f));
        colors.put(19, new Color(0.95f, 0.95f, 0.95f, 1f));
        colors.put(18, new Color(0.9f, 0.9f, 0.9f, 1f));
        colors.put(17, new Color(0.85f, 0.85f, 0.85f, 1f));
        colors.put(16, new Color(0.8f, 0.8f, 0.8f, 1f));
        colors.put(15, new Color(0.75f, 0.75f, 0.75f, 1f));
        colors.put(14, new Color(0.7f, 0.7f, 0.7f, 1f));
        colors.put(13, new Color(0.65f, 0.65f, 0.65f, 1f));
        colors.put(12, new Color(0.6f, 0.6f, 0.6f, 1f));
        colors.put(11, new Color(0.55f, 0.55f, 0.55f, 1f));
        colors.put(10, new Color(0.5f, 0.5f, 0.5f, 1f));
        colors.put(9, new Color(0.45f, 0.45f, 0.5f, 1f));
        colors.put(8, new Color(0.4f, 0.4f, 0.5f, 1f));
        colors.put(7, new Color(0.35f, 0.35f, 0.5f, 1f));
        colors.put(6, new Color(0.3f, 0.3f, 0.5f, 1f));
        colors.put(5, new Color(0.25f, 0.25f, 0.5f, 1f));
        colors.put(4, new Color(0.2f, 0.2f, 0.45f, 1f));
        colors.put(3, new Color(0.15f, 0.15f, 0.4f, 1f));
        colors.put(2, new Color(0.1f, 0.1f, 0.3f, 1f));
        colors.put(1, new Color(0.05f, 0.05f, 0.2f, 1f));
        colors.put(0, new Color(0, 0f, 0f, 0f));


    }

    public static void addScore(int scoreToAdd){
        score += scoreToAdd;
    }

    public static int getDifficult() {
        return entityController.getDifficult();
    }

    public int getScore() {
        return score;
    }

    private void setupCrosshair() {
        crossHair = new Sprite(GameTexture.crossHair);
        crossHair.setSize(16, 16);
    }

    public static void addBullet(Bullet bullet, Vector2 postion, Vector2 direction) {
        entityController.addBullet(bullet, postion, direction);
    }

    public static void addParticles(Array<Particle> particles, Vector2 postion, int number) {
        entityController.addParticles(particles, postion, number);
    }

    public static void addParticle(Particle particle) {
        entityController.addParticle(particle);
    }

    private void setupBackround() {

        this.ground = new Sprite(GameTexture.smallWorld.findRegion("1"));
        //this.ground.setSize(Ld38.WIDTH * 2, Ld38.HEIGHT);
        this.ground.setSize(Ld38.WIDTH + 250, Ld38.HEIGHT);
        this.ground.translate(-125, 0);

        this.stars = new Sprite(GameTexture.stars);

        this.stars.setSize(Ld38.WIDTH + 250, 200);
        this.stars.setPosition(0, Ld38.HEIGHT - 200);

        this.sun = new Sprite(GameTexture.sun);

        this.sun.setSize(256, 256);
        this.sun.setPosition(Ld38.WIDTH - 300, Ld38.HEIGHT - 300);

        float x = this.ground.getX();
        float y = this.ground.getY();

        this.groundNormalPosition = new Vector2(x,y);

        //this.ground.setX(0 - Ld38.WIDTH / 2);

        // this.ground2 = new Sprite(ground);
        //this.ground.setSize(Ld38.WIDTH * 2, Ld38.HEIGHT);
        // this.ground2.setSize(Ld38.WIDTH, Ld38.HEIGHT);
        // this.ground2.setAlpha(0.5f);

        //this.ground.setX(0 - Ld38.WIDTH / 2);

    }

    public void tick(float delta) {


        ground.setPosition(groundNormalPosition.x, groundNormalPosition.y);
        entityController.tick(delta);

        float x = 0;


        //System.out.println(player.getX());

        if (player.getX() < Ld38.WIDTH - 52) {
            if (inputController.isRightDown()) {
                x += delta * 200;
                ground.translate(-x / 3, 0);
                stars.translate(-x / 12, 0);
                sun.translate(-x / 6, 0);
                groundNormalPosition.set(ground.getX(), ground.getY());
                //ground.scroll(x / 2000, 0);
                //ground2.scroll(x / 1000, 0);

                player.translate(x, 0);
            }
        }
        if (player.getX() > 9) {
            if (inputController.isLeftDown()) {
                x -= delta * 200;


                // ground.scroll(x / 2000, 0);
                //ground2.scroll(x / 1000, 0);
                ground.translate(-x / 3, 0);
                stars.translate(-x / 12, 0);
                sun.translate(-x / 6, 0);

                groundNormalPosition.set(ground.getX(), ground.getY());
                player.translate(x, 0);
            }

        }

        if (ground.getX() > 8f){
            ground.setPosition(8f, ground.getY());
            groundNormalPosition.set(ground.getX(), ground.getY());
        }

        if (ground.getX() < -239f){
            ground.setPosition(-239f, ground.getY());
            groundNormalPosition.set(ground.getX(), ground.getY());
        }



        //System.out.println(ground.getX());


        shootTimeout -= delta;

        //System.out.println(shootTimeout);
        if (inputController.isLeftButtonPressed() && shootTimeout <= 0.0f) {
            float v = MathUtils.atan2(crossHairLocation.y - player.getY(), crossHairLocation.x - player.getX());
            Vector2 direction = new Vector2(MathUtils.cosDeg(MathUtils.radiansToDegrees * v), MathUtils.sinDeg(MathUtils.radiansToDegrees * v));
            GameSound.shoot.play(0.5f);
            if (player.hasDoubleShoot() && !player.hasTripleShoot()) {
                addBullet(new SmallBullet(player, 300, 6, Color.WHITE), new Vector2(player.getX() + 12, player.getY() + 24), direction.cpy().add(-0.02f, 0).nor());
                addBullet(new SmallBullet(player, 300, 6, Color.WHITE), new Vector2(player.getX() + 44, player.getY() + 24), direction.cpy().add(+0.02f, 0).nor());
            } else if (player.hasTripleShoot()) {
                addBullet(new SmallBullet(player, 300, 6, Color.WHITE), new Vector2(player.getX() + 12, player.getY() + 24), direction.cpy().add(-0.05f, 0).nor());
                addBullet(new SmallBullet(player, 300, 6, Color.WHITE), new Vector2(player.getX() + 28, player.getY() + 24), direction.cpy().nor());
               addBullet(new SmallBullet(player, 300, 6, Color.WHITE), new Vector2(player.getX() + 44, player.getY() + 24), direction.cpy().add(+0.05f, 0).nor());
            } else {
                addBullet(new SmallBullet(player, 300, 6, Color.WHITE), new Vector2(player.getX() + 28, player.getY() + 24), direction.nor());
            }

            if (player.hasBulletSpeed()) {
                shootTimeout = 0.1f;
            } else {
                shootTimeout = 2.0f;
            }
        }


        player.tick(delta);

        if (worldHealth != previousWorldHealth) {
            ground.setColor(colors.get(worldHealth));
            previousWorldHealth = worldHealth;
        }


        if (worldShake > 0.0f){
            shakeX = MathUtils.sin(worldShake)*MathUtils.random(1,14);
            shakeY = MathUtils.cos(worldShake)*MathUtils.random(1,14);
            //ground.translate(shakeX, shakeY);

            worldShake /= 1.05f;

            if (worldShake < 0.005f){
                worldShake = 0.0f;
                shakeX = 0.0f;
                shakeY = 0.0f;
            }
        }

        handleCrosshair();


    }

    private void handleCrosshair() {

        int screenHeight = Gdx.graphics.getHeight();
        int screenWidth = Gdx.graphics.getWidth();

        float heightRatio = screenHeight/Ld38.HEIGHT;
        float widthRatio = screenWidth/Ld38.WIDTH;

        crossHairLocation.set(inputController.getMouseLocationX()/widthRatio, (Gdx.graphics.getHeight() - inputController.getMouseLocationY())/heightRatio);

        if (crossHairLocation.x < 0) crossHairLocation.x = 0;
        if (crossHairLocation.x > Ld38.WIDTH) crossHairLocation.x = Ld38.WIDTH;

        if (crossHairLocation.y < 0) crossHairLocation.y = 0;
        if (crossHairLocation.y > Ld38.HEIGHT) crossHairLocation.y = Ld38.HEIGHT;

    }

    public void render(SpriteBatch spriteBatch) {

        ground.translate(shakeX, shakeY);
        ground.draw(spriteBatch);
        ground.translate(-shakeX, -shakeY);
        stars.draw(spriteBatch);
        sun.draw(spriteBatch);

        //ground2.draw(spriteBatch);

        //backgroundController.render(spriteBatch);


        entityController.render(spriteBatch);

        player.render(spriteBatch);
        entityController.renderPlayerShield(spriteBatch);

        crossHair.setPosition(crossHairLocation.x,crossHairLocation.y);
        crossHair.draw(spriteBatch);

    }

    public static void hurtWorld() {
        worldHealth--;
        worldShake = 5.0f;

    }


    public static void addEntity(Entity entity, Vector2 vector2) {
        entityController.addEntity(entity, vector2);
    }

    public boolean worldIsDead() {
        return worldHealth <= 0;
    }


    public void dispose() {
        this.player = null;
    }
}
