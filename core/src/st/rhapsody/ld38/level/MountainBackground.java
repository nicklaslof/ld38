package st.rhapsody.ld38.level;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld38.Ld38;
import st.rhapsody.ld38.resource.GameTexture;

/**
 * Created by nicklas on 2017-04-23.
 */
public class MountainBackground extends Background{

    public MountainBackground(Texture texture, Vector2 position, float yOffset) {
        super(new Sprite(texture), position, yOffset);

    }

    @Override
    public Bezier<Vector2> generateBezier() {
        return new Bezier<Vector2>(new Vector2(-320, -80), new Vector2((Ld38.WIDTH / 2f), (Ld38.HEIGHT/4)+130), new Vector2(Ld38.WIDTH+320, -80));
    }
}
