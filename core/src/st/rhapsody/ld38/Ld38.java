package st.rhapsody.ld38;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import st.rhapsody.ld38.resource.GameSound;
import st.rhapsody.ld38.resource.GameTexture;
import st.rhapsody.ld38.screen.GameScreen;
import st.rhapsody.ld38.screen.GameoverScreen;
import st.rhapsody.ld38.screen.IntroScreen;

public class Ld38 extends Game {

    private static Screen previousScreen;
    private static Screen currentScreen;
    private static boolean changeScreen;


    public static int WIDTH = 800;
    public static int HEIGHT = 600;
    private GameTexture gameTexture;
    private Font font;
    private GameSound gameSound;

    @Override
    public void create() {

        gameTexture = new GameTexture();
        gameSound = new GameSound();
        font = new Font();
        setIntroScreen();

        GameSound.song.loop(1.0f);
    }

    @Override
    public void render() {
        super.render();
        if (changeScreen) {
            setScreen(currentScreen);
            changeScreen = false;
        }

        screen.render(Gdx.graphics.getDeltaTime());


    }

    public static void setGameoverScreen(GameoverScreen.Type type, int finalScore){
        previousScreen = currentScreen;
        dispose(previousScreen);
        currentScreen = new GameoverScreen(type, finalScore);
        changeScreen = true;
    }

    public static void setGameScreen() {
        previousScreen = currentScreen;
        dispose(previousScreen);
        currentScreen = new GameScreen();
        changeScreen = true;
    }

    public static void setIntroScreen() {
        previousScreen = currentScreen;
        dispose(previousScreen);
        currentScreen = new IntroScreen();
        changeScreen = true;
    }

    private static void dispose(Screen screen) {
        if (screen != null) {
            screen.dispose();
        }
    }


}
