package st.rhapsody.ld38;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by nicklas on 2017-04-22.
 */
public class Font {
    private static GlyphLayout glyphLayout;
    private static BitmapFont bitmapFont;

    public Font() {
        glyphLayout = new GlyphLayout();
        bitmapFont = new BitmapFont();
    }

    public static void renderText(String text, float x, float y, SpriteBatch spriteBatch){
        glyphLayout.setText(bitmapFont,text);
        bitmapFont.draw(spriteBatch,text,x, y);
    }



}
