package st.rhapsody.ld38.resource;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Created by nicklas on 2017-04-22.
 */
public class GameTexture {

    private final AssetManager assetManager;
    public static Texture smallBullet;
    public static Texture crossHair;
    public static TextureAtlas smallWorld;
    public static Texture ufo;
    public static Texture bomber;
    public static Texture bomb;
    public static Texture pinky;
    public static Texture seeker;
    public static Texture stars;
    public static Texture sun;
    public static Texture pixel;

    public static Texture bubble;

    public static Texture doubleShootPower;
    public static Texture healthPower;
    public static Texture speedPower;
    public static Texture tripleShootPower;


    public static Texture hero;

    public GameTexture() {

        assetManager = new AssetManager();

        assetManager.load("bullet.png", Texture.class);
        assetManager.load("cross.png", Texture.class);
        assetManager.load("small_world.pack", TextureAtlas.class);
        assetManager.load("ufo.png", Texture.class);
        assetManager.load("hero.png", Texture.class);
        assetManager.load("bomber.png", Texture.class);
        assetManager.load("bomb.png", Texture.class);
        assetManager.load("pinky.png", Texture.class);
        assetManager.load("seeker.png", Texture.class);
        assetManager.load("stars.png", Texture.class);
        assetManager.load("sun.png", Texture.class);

        assetManager.load("pixel.png", Texture.class);

        assetManager.load("bubble.png", Texture.class);

        assetManager.load("double_shoot_power.png", Texture.class);
        assetManager.load("health_power.png", Texture.class);
        assetManager.load("speed_power.png", Texture.class);
        assetManager.load("triple_shoot_power.png", Texture.class);

        assetManager.finishLoading();


        smallBullet = assetManager.get("bullet.png", Texture.class);
        crossHair = assetManager.get("cross.png", Texture.class);
        smallWorld = assetManager.get("small_world.pack", TextureAtlas.class);
        //smallWorld.setWrap(Texture.TextureWrap.ClampToEdge, Texture.TextureWrap.ClampToEdge);
        ufo = assetManager.get("ufo.png", Texture.class);
        hero = assetManager.get("hero.png", Texture.class);
        bomber = assetManager.get("bomber.png", Texture.class);
        bomb = assetManager.get("bomb.png", Texture.class);
        pinky = assetManager.get("pinky.png", Texture.class);
        seeker = assetManager.get("seeker.png", Texture.class);
        stars = assetManager.get("stars.png", Texture.class);
        sun = assetManager.get("sun.png", Texture.class);

        pixel = assetManager.get("pixel.png", Texture.class);


        bubble = assetManager.get("bubble.png", Texture.class);

        doubleShootPower = assetManager.get("double_shoot_power.png", Texture.class);
        healthPower = assetManager.get("health_power.png", Texture.class);
        speedPower = assetManager.get("speed_power.png", Texture.class);
        tripleShootPower = assetManager.get("triple_shoot_power.png", Texture.class);
    }
}
