package st.rhapsody.ld38.resource;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by nicklas on 2017-04-24.
 */
public class GameSound {

    private final AssetManager assetManager;

    public static Sound song;
    public static Sound worldHit;
    public static Sound health;
    public static Sound explode;
    public static Sound shoot;
    public static Sound playerHurt;
    public static Sound weapon;
    public static Sound speed;
    public static Sound shieldProtect;
    public static Sound gameOver;
    public static Sound rocket;
    public static Sound partialHurt;


    public GameSound() {

        assetManager = new AssetManager();
        assetManager.load("song.mp3", Sound.class);
        assetManager.load("world_hit.mp3", Sound.class);
        assetManager.load("health.mp3", Sound.class);
        assetManager.load("explode.mp3", Sound.class);
        assetManager.load("shoot.mp3", Sound.class);
        assetManager.load("player_hurt.mp3", Sound.class);
        assetManager.load("weapon.mp3", Sound.class);
        assetManager.load("speed.mp3", Sound.class);
        assetManager.load("shield_protect.mp3", Sound.class);
        assetManager.load("gameover.mp3", Sound.class);
        assetManager.load("rocket.mp3", Sound.class);
        assetManager.load("partialhurt.mp3", Sound.class);

        assetManager.finishLoading();


        song = assetManager.get("song.mp3", Sound.class);
        worldHit = assetManager.get("world_hit.mp3", Sound.class);
        health = assetManager.get("health.mp3", Sound.class);
        explode = assetManager.get("explode.mp3", Sound.class);
        shoot = assetManager.get("shoot.mp3", Sound.class);
        playerHurt = assetManager.get("player_hurt.mp3", Sound.class);
        weapon = assetManager.get("weapon.mp3", Sound.class);
        shieldProtect = assetManager.get("shield_protect.mp3", Sound.class);
        speed = assetManager.get("speed.mp3", Sound.class);
        gameOver = assetManager.get("gameover.mp3", Sound.class);
        rocket = assetManager.get("rocket.mp3", Sound.class);
        partialHurt = assetManager.get("partialhurt.mp3", Sound.class);



    }
}
