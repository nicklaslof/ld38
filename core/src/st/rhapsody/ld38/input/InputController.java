package st.rhapsody.ld38.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import st.rhapsody.ld38.Ld38;

/**
 * Created by nicklas on 2017-04-22.
 */
public class InputController implements InputProcessor {
    private boolean leftDown;
    private boolean rightDown;
    private int mouseLocationX;
    private int mouseLocationY;
    private boolean leftButtonPressed;
    private boolean gamePaused;


    @Override
    public boolean keyDown(int keycode) {
        boolean processed = false;
        switch (keycode) {
            case Input.Keys.A:
            case Input.Keys.LEFT:
                leftDown = true;
                processed = true;
                break;
            case Input.Keys.D:
            case Input.Keys.RIGHT:
                rightDown = true;
                processed = true;
                break;
            case Input.Keys.SPACE:
                leftButtonPressed = true;
                processed = true;
                break;
            case Input.Keys.ESCAPE:
            case Input.Keys.P:
                Gdx.input.setCursorCatched(false);
                processed = true;
                gamePaused = true;
        }
        return processed;
    }

    @Override
    public boolean keyUp(int keycode) {
        boolean processed = false;
        switch (keycode) {
            case Input.Keys.A:
            case Input.Keys.LEFT:
                leftDown = false;
                processed = true;
                break;
            case Input.Keys.D:
            case Input.Keys.RIGHT:
                rightDown = false;
                processed = true;
                break;
            case Input.Keys.SPACE:
                leftButtonPressed = false;
                processed = true;
                break;
        }
        return processed;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        boolean processed = false;
        if (button == Input.Buttons.LEFT) {
            if (gamePaused) {
                Gdx.input.setCursorCatched(true);
                gamePaused = false;
                leftButtonPressed = true;
            }else{
                leftButtonPressed = true;
            }
            processed = true;
        }


        return processed;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        boolean processed = false;
        if (button == Input.Buttons.LEFT) {
            leftButtonPressed = false;
            processed = true;
        }


        return processed;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        mouseLocationX = screenX;
        mouseLocationY = screenY;
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        mouseLocationX = screenX;
        mouseLocationY = screenY;
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public boolean isLeftDown() {
        return leftDown;
    }

    public boolean isRightDown() {
        return rightDown;
    }

    public boolean isLeftButtonPressed() {
        return leftButtonPressed;
    }

    public int getMouseLocationX() {
        return mouseLocationX;
    }

    public int getMouseLocationY() {
        return mouseLocationY;
    }

    public boolean isGamePaused() {
        return gamePaused;
    }
}
